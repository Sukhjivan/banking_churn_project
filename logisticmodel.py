#%% Imports
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_curve, roc_auc_score
from sklearn.calibration import calibration_curve
from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_confusion_matrix
#%% monthly data
 
file_ = 'final.csv'
df = pd.read_csv(file_)
df.drop(columns=['Unnamed: 0','final_trans_date_me'], inplace=True)
df.fillna(0,inplace=True) 
df['churns_next_month'] = df['churns_next_month'].astype(int)
 
df['date'] = pd.to_datetime(df['date'])
 
end_date = pd.to_datetime('2020-05-31')
mask = df['date'] < end_date
df = df[mask]

df_train, df_test = train_test_split(df, test_size=0.2, stratify=df['churns_next_month'], random_state=1)
 
#%%
target = 'churns_next_month'
predictors = ['sum_transactions', 'EFFR', 'bal_change', 'start_balance', 'deposit_3month_proportion','deposit_lifetime_proportion']
 
# balance change is the change in the balance as a percentage of their total balance
# sum of all the transactions for that month
 
X_test = df_test[predictors]
y_test = df_test[target]
 
X_train = df_train[predictors]
y_train = df_train[target]
 
model = LogisticRegression()
model.fit(X_train, y_train)
 
print(model)
 
y_hat_hard = model.predict(X_test)
y_hat_soft = model.predict_proba(X_test)
 
accuracy = accuracy_score(y_test, y_hat_hard)
print("Accuracy: %.2f%%" % (accuracy * 100.0))
 
# %%
#from xgboost import plot_importance
#import matplotlib.pyplot as plt
 
# plot feature importance
#plot_importance(model)
#plt.show()
 
#%%
# Model evaluation
#ROC Curve

fpr, tpr, thresholds = roc_curve(y_test,y_hat_soft[:,1], pos_label = 1)
fig, ax = plt.subplots()
ax.plot(fpr,tpr)
ax.set_xlabel('False positive rate')
ax.set_ylabel('True positive rate')
ax.set_title('ROC curve')

AUC = roc_auc_score(y_test,y_hat_soft[:,1])
print('ROC AUC Score:', AUC)
 
print(thresholds)
 
# callibration curve
 
prob_true, prob_pred = calibration_curve(y_test,y_hat_soft[:,1])
fig, ax = plt.subplots()


ax.plot([0,1],[0,1], label = 'ideal line', c = 'red')
ax.plot(prob_pred, prob_true, label = 'model line', c = 'blue', marker='o')
ax.set_title('Calibration Curve')
ax.set_xlabel('Predictions')
ax.set_ylabel('Proportion of Positive Observationsean ')
plt.legend()
 
# %%
# predicting on May data
 
file_ = 'final_af.csv'
df = pd.read_csv(file_)
df.drop(columns=['Unnamed: 0','final_trans_date_me'], inplace=True)
df.fillna(0,inplace=True)
df['churns_next_month'] = df['churns_next_month'].astype(int)
 
df['date'] = pd.to_datetime(df['date'])
 
end_date = pd.to_datetime('2020-05-31')
mask = df['date'] == end_date
df = df[mask]
 
pred = model.predict_proba(df[predictors])[:,1]
df_sub = pd.DataFrame()
df_sub['account_id'] = df['account_id']
df_sub['date'] = '31/05/2020'
df_sub['pred'] = pred
 
df_sub.to_csv('logistic_submission_theia.csv')
# %%
confusion_matrix = plot_confusion_matrix(model, X_test, y_test, cmap = 'summer')
# %%
