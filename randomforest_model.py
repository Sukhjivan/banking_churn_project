# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 17:46:00 2021

@author: DidiSadeq
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 14:07:35 2021

@author: DidiSadeq
"""
#%%
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import metrics
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.calibration import calibration_curve
#%%

customer_filepath = r'C:/Users/DidiSadeq/Documents/PythonScripts/ML/Customer Churn/final.csv'
df= pd.read_csv(customer_filepath)


#%%


target     = 'churns_next_month'
predictors=['bal_change','sum_transactions']


df_train, df_test = train_test_split(df, test_size=0.2, stratify=df['churns_next_month'], random_state=123)

 



#%%
# Fit model
model = RandomForestClassifier()
model.fit(df_train[predictors], df_train[target])

 

#predicting test set results and calculating accuracy
y_pred_hard= model.predict(df_test[predictors])
y_pred_soft=model.predict_proba(df_test[predictors])



#accuracy of score:
accuracy=accuracy_score(df_test[target],y_pred_hard)
print('Accuracy of hard prediction:',accuracy)

#%%

plt.figure()
metrics.plot_roc_curve(model, df_test[predictors],df_test[target])
plt.xlabel('1-TNR')
plt.ylabel('TPR')
plt.show()
#%%
print(model.get_params())


