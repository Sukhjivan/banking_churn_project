#%% Imports
import pandas as pd
import numpy as np
import datetime as dt
from pandas.tseries.offsets import MonthEnd

#%% Import csv
filepath = 'Clean_merged_data.csv'
df_merge = pd.read_csv(filepath)
df_merge.drop(columns = 'Unnamed: 0', inplace = True)
# %%
df_new = pd.DataFrame(df_merge.groupby(['customer_id','date'])['state'].first()).reset_index()

# %%
starting_balances = pd.DataFrame(df_merge.groupby(['customer_id','date'])['start_balance'].first()).reset_index()

account_ids = pd.DataFrame(df_merge.groupby(['customer_id','date'])['account_id'].first()).reset_index()

sum_transactions = pd.DataFrame(df_merge.groupby(['customer_id','date'])[['amount','deposit','withdrawal']].sum()).reset_index()
sum_transactions.rename(columns={"amount":"sum_transactions","deposit":"sum_deposit","withdrawal":"sum_withdrawal"}, inplace = True)

count_transactions =  pd.DataFrame(df_merge.groupby(['customer_id','date'])[['amount','deposit','withdrawal']].agg(np.count_nonzero)).reset_index()
count_transactions['deposit_proportion'] = count_transactions['deposit']/count_transactions['amount']
count_transactions['withdrawal_proportion'] = count_transactions['withdrawal']/count_transactions['amount']
#count_transactions.drop(columns = ['deposit','withdrawal'], inplace=True)


# %%
count_transactions['cumsum_amount'] = count_transactions.groupby('customer_id')['amount'].transform(pd.Series.cumsum)
count_transactions['cumsum_deposit'] = count_transactions.groupby('customer_id')['deposit'].transform(pd.Series.cumsum)
count_transactions['cumsum_withdrawal'] = count_transactions.groupby('customer_id')['withdrawal'].transform(pd.Series.cumsum)
# %%
count_transactions['deposit_lifetime_proportion'] = count_transactions['cumsum_deposit']/count_transactions['cumsum_amount']
count_transactions['withdrawal_lifetime_proportion'] = count_transactions['cumsum_withdrawal']/count_transactions['cumsum_amount']

# %%
count_transactions['3month_amount'] = count_transactions.groupby('customer_id')['amount'].transform(lambda s: s.rolling(3, min_periods=1).sum())
count_transactions['3month_deposit'] = count_transactions.groupby('customer_id')['deposit'].transform(lambda s: s.rolling(3, min_periods=1).sum())
count_transactions['3month_withdrawal'] = count_transactions.groupby('customer_id')['withdrawal'].transform(lambda s: s.rolling(3, min_periods=1).sum())

# %%
count_transactions['deposit_3month_proportion'] = count_transactions['3month_deposit']/count_transactions['3month_amount']
count_transactions['withdrawal_3month_proportion'] = count_transactions['3month_withdrawal']/count_transactions['3month_amount']
count_transactions.rename(columns={"amount":"number_of_transactions"}, inplace = True)
# %%
count_transactions.drop(columns = ['deposit','withdrawal', 'cumsum_amount','cumsum_deposit','cumsum_withdrawal','3month_amount','3month_deposit','3month_withdrawal'], inplace=True)
#%% 
df = pd.merge(df_new,account_ids, on = ['customer_id', 'date'])
# %%
df = pd.merge(df,starting_balances, on = ['customer_id', 'date'])
df = pd.merge(df,sum_transactions, on = ['customer_id', 'date'])
df = pd.merge(df,count_transactions, on = ['customer_id', 'date'])


# %% interest rates
df['date'] = pd.to_datetime(df['date'])

df['DATE'] = df['date'].apply(lambda dt: dt.replace(day=1))

df['DATE'] = df['DATE'].dt.strftime('%Y-%m-%d')
# %%
EFFR_filepath = 'EFFR.csv'
df_effr = pd.read_csv(EFFR_filepath)
df = pd.merge(df, df_effr, on='DATE')

prime_filepath = 'bank_prime_loan_rate.csv'
df_prime = pd.read_csv(prime_filepath)
df = pd.merge(df, df_prime, on='DATE')

treasury_filepath = '3Month_Treasury_Constant_Maturity_Rate.csv'
df_treas = pd.read_csv(treasury_filepath)
df = pd.merge(df, df_treas, on='DATE')
# %%
df.drop(columns='DATE', inplace = True)

df.sort_values(['customer_id','date'], inplace=True)

df.to_csv('monthly_database.csv')

#%%




