# %% Labelling

import pandas as pd
import numpy as np
import datetime as dt
from pandas.tseries.offsets import MonthEnd
# Change In Balance Column
df_new = pd.read_csv('monthly_database.csv')
df_new['date'] = pd.to_datetime(df_new['date'])
df_new.drop(columns='Unnamed: 0', inplace=True)
df_new['cumsum'] = df_new.groupby(['customer_id'])['sum_transactions'].transform(pd.Series.cumsum)
df_new['cumsum'] = df_new['cumsum']+df_new['start_balance']
df_new['bal_change'] = df_new.groupby(['customer_id'])['cumsum'].transform(pd.Series.pct_change)
df_new.replace(np.inf, 1., inplace=True) #inf when balance added to 0 balance
df_new.replace(-np.inf,-1.,inplace=True) # -inf when +ve balance and everything removed
df_new.drop(columns='Unnamed: 0', inplace=True)
df_new['bal_change'].fillna(0, inplace=True)
df_new2 = df_new

# Churns Next Month Column
last_trans = pd.read_csv('first_last_df.csv')
last_trans['final_trans_date'] = pd.to_datetime(last_trans['final_trans_date'], format='%Y-%m')
last_trans['final_trans_date_me'] = pd.to_datetime(last_trans['final_trans_date'], format="%Y%m") + MonthEnd(1)-MonthEnd(1)
last_trans = last_trans[['customer_id','final_trans_date','final_trans_date_me']]
df_new2 = pd.merge(df_new2,last_trans, on='customer_id', how='left')
df_new2['date'] = pd.to_datetime(df_new2['date'])
df_new2['final_trans_date_me'] = pd.to_datetime(df_new2['final_trans_date_me'])
df_new2['churns_next_month'] = df_new2['date'] == df_new2['final_trans_date_me']
df_new2.to_csv('final.csv')


