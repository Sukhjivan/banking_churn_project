'''
Author : Tom Gurman
Created: 2021-04-12
 
Overview
--------
In this script, we will pre-process the data and check the integrity to ensure it is fit for purpose.

'''

#%% Imports
import pandas as pd
import numpy as np
import seaborn as sns


# %% Customer data exploration
customer_filepath = 'customers_tm1_e.csv'
df_cust = pd.read_csv(customer_filepath)


df_cust.info()
df_cust.head()

#%%Transaction Data Exploration
transaction_filepath = 'transactions_tm1_e.csv'
df_trans = pd.read_csv(transaction_filepath)

df_group = pd.DataFrame(df_trans.groupby(['customer_id','date'])['amount'].first()).reset_index()
df_group['date'] = pd.to_datetime(df_group['date'])
 
end_date = pd.to_datetime('2020-05-31')
mask = df_group['date'] == end_date
df_may = df_group[mask]




# %% Correct State Variations
df_cust.loc[(df_cust.state =='TX'),'state']='Texas'
df_cust.loc[(df_cust.state =='NY'),'state']='New York'
df_cust.loc[(df_cust.state =='MASS'),'state']='Massachusetts'
df_cust.loc[(df_cust.state =='CALIFORNIA'),'state']='California'
df_cust.loc[(df_cust.state =='UNK'),'state']='Nebraska'


# %% Combine the dataframes
df = pd.merge(df_cust, df_trans, on="customer_id")

# %% Removing australian customers
state_mask = df['state'] != 'Australia'
df = df[state_mask]


df_group_merge = pd.DataFrame(df.groupby(['customer_id','date'])['amount'].first()).reset_index()
df_group_merge['date'] = pd.to_datetime(df_group_merge['date'])
 
end_date = pd.to_datetime('2020-05-31')
mask = df_group_merge['date'] == end_date
df_may_merge = df_group_merge[mask]

#%%drop the broken amounts column  
df.drop(columns = 'amount', inplace=True)
#%% Create a new amounts colum
df['amount'] = df['deposit']+df['withdrawal']


# %% Removing non-transactions
# amount_mask = df['amount'] != 0.0
# df = df[amount_mask]

# %% Removing Billionaire Outliers
start_balance_mask = abs(df['start_balance'])<=1e+6
df = df[start_balance_mask]


df_group_merge_clean = pd.DataFrame(df.groupby(['customer_id','date'])['amount'].first()).reset_index()
df_group_merge_clean['date'] = pd.to_datetime(df_group_merge_clean['date'])
 
end_date = pd.to_datetime('2020-05-31')
mask = df_group_merge_clean['date'] == end_date
df_may_merge_clean = df_group_merge_clean[mask]





# %% Saving to csv
df.to_csv('Clean_merged_data_with_non_trans.csv')



# %%
