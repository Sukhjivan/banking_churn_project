'''

Author: Sarah James
Created: 15-04-2021

'''
#%%
# Imports

from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import pandas as pd

#%%
# monthly data

file_ = r'.\final.csv'
df = pd.read_csv(file_)
df.drop(columns=['Unnamed: 0', 'final_trans_date_me'], inplace=True)
df['churns_next_month'] = df['churns_next_month'].astype(int)
df.fillna(0,inplace=True)

df['date'] = pd.to_datetime(df['date'])

end_date = pd.to_datetime('2020-05-31')
mask = df['date'] < end_date
df = df[mask]

df_train, df_test = train_test_split(df, test_size=0.2, stratify=df['churns_next_month'], random_state=1)

# %%
# Feature importance
# this requires having a large set of predictors defined in the previous cell
# after seeing the results of the plot below for most features, we picked the above 3

from xgboost import plot_importance
import matplotlib.pyplot as plt

target = 'churns_next_month'
predictors = [ 'start_balance', 'sum_transactions', 'sum_deposit', 'sum_withdrawal',
       'number_of_transactions', 'deposit_proportion', 'withdrawal_proportion',
       'deposit_lifetime_proportion', 'withdrawal_lifetime_proportion',
       'deposit_3month_proportion', 'withdrawal_3month_proportion', 'EFFR',
       'bank_prime_loan_rate', 'DGS3MO', 'cumsum', 'bal_change']
# balance change is the change in the balance as a percentage of their total balance
# sum of all the transactions for that month

X_test = df_test[predictors]
y_test = df_test[target]

X_train = df_train[predictors]
y_train = df_train[target]

model = XGBClassifier(max_depth = 3, n_estimators=100)
model.fit(X_train, y_train)

# plot feature importance
plot_importance(model)
plt.show()

#%%
# Training model

target = 'churns_next_month'
predictors = ['sum_transactions','deposit_3month_proportion', 'EFFR', 'bal_change']
# balance change is the change in the balance as a percentage of their total balance
# sum of all the transactions for that month

X_test = df_test[predictors]
y_test = df_test[target]

X_train = df_train[predictors]
y_train = df_train[target]

model = XGBClassifier(max_depth = 3, n_estimators=100)
model.fit(X_train, y_train)

print(model)

y_pred_prob = model.predict_proba(X_test)
y_pred = model.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print("Accuracy: %.2f%%" % (accuracy * 100.0))


#%%
# Model evaluation

from sklearn.metrics import roc_curve, roc_auc_score, confusion_matrix, plot_confusion_matrix
from sklearn.calibration import calibration_curve

# ROC, AUC
y_hat_prob = model.predict_proba(X_test)[:,1]

auc = roc_auc_score(y_test, y_hat_prob)
print('area under curve is ', auc)

fpr, tpr, thresholds = roc_curve(y_test, y_hat_prob)

fig, ax = plt.subplots()
ax.plot(fpr, tpr)
ax.set_xlabel('FPR (=1-TNR)')
ax.set_ylabel('TPR')
ax.set_title('ROC curve')

print(thresholds)

# callibration curve

prob_true, prob_pred = calibration_curve(y_test, y_hat_prob, n_bins=7)

fig, ax = plt.subplots()
ax.plot(prob_pred, prob_true, marker ='o', c = 'green')
ax.plot([0,1], [0, 1], c='grey')
ax.set_xlabel('predicted probability')
ax.set_ylabel('proportion of positive observations')
ax.set_title('calibration curve')

# confusion matrix

confusion_matrix = plot_confusion_matrix(model, X_test, y_test, cmap = 'summer')

# %%
# predicting on May data

file_ = r'C:\Users\SarahJames\OneDrive - Kubrick Group\Kubrick Group\Banking Customer Churn Project\final_af.csv'
df = pd.read_csv(file_)
df.drop(columns='Unnamed: 0', inplace=True)
df['churns_next_month'] = df['churns_next_month'].astype(int)

df['date'] = pd.to_datetime(df['date'])

end_date = pd.to_datetime('2020-05-31')
mask = df['date'] == end_date
df = df[mask]

pred = model.predict_proba(df[predictors])[:,1]
df_sub = pd.DataFrame()
df_sub['account_id'] = df['account_id']
df_sub['date'] = '31/05/2020'
df_sub['pred'] = pred

df_sub.to_csv('submission_theia.csv')