import pandas as pd 
import numpy as np 

file_path = r'C:\Users\SukhjivanSandhu\Python\banking_churn\clean_merged_data.csv'
df = pd.read_csv(file_path)
df.drop(columns = 'Unnamed: 0', inplace=True)


df[['total_transactions', 'total_deposits', 'total_withdrawals']] = df.groupby('customer_id')[['amount','deposit','withdrawal']].transform(np.count_nonzero)


groupby_df = df.groupby('customer_id',as_index=False)
diff_df = pd.DataFrame(columns=['customer_id','transaction_date','amount']) #empty df
 
for name,group in groupby_df:
    print(name)
    group = group[['customer_id','transaction_date','amount']]
    group = group.sort_values(by='transaction_date')
    group['diff'] = group['transaction_date'] - group['transaction_date'].shift(1)
    diff_df = diff_df.append(group,ignore_index=True)
 
df = pd.merge(df,diff_df,how='left',on=['customer_id','transaction_date','amount'])
df = df.sort_values(by=['customer_id','transaction_date'])


# For every customer_id, want the total number of transactions
# list of customer ids
customer_ids = list(set([i for i in df['customer_id']]))
trans_counts = {}
# In the range of each customers number of transactions, calculate the date between each transaction
for i in customer_ids:
    trans_counts[i] = list(df[df['customer_id'] == i]['total_transactions'])[0]
trans_counts_df = pd.DataFrame.from_dict(trans_counts, orient='index', columns = ['total_transactions']).reset_index().rename(columns={'index':'customer_id'})

# Find the index for the least transaction of each customer
trans_index = trans_counts_df['total_transactions'] - 1
trans_index

# Sort dataframe in order of customer_id and transaction_date
df.sort_values(['customer_id', 'transaction_date'], inplace=True)
final_trans_dates = {}
for i,j in tqdm(zip(customer_ids, trans_index)):
    final_trans_date = list(df[df['customer_id'] == i]['transaction_date'])[j]
    final_trans_dates[i] = [final_trans_date]
# Write results to a dataframe
final_trans_df = pd.DataFrame.from_dict(final_trans_dates, orient='index', columns = ['final_trans_date']).reset_index().rename(columns={'index':'customer_id'})


for i in tqdm(customer_ids):
    first_trans_date = list(df[df['customer_id'] == i]['transaction_date'])[0]
    first_trans_dates[i] = [final_trans_date]
# Write results to a dataframe
first_trans_df = pd.DataFrame.from_dict(first_trans_dates, orient='index', columns = ['first_trans_date']).reset_index().rename(columns={'index':'customer_id'})

# Concat df's together
first_last_df = pd.concat([first_trans_df, final_trans_df.drop(columns='customer_id')], axis=1)
first_last_df.to_csv(r'C:\Users\SukhjivanSandhu\Python\banking_churn\first_last_df.csv')
